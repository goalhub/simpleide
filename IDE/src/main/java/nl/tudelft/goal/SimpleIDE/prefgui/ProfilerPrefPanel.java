/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.prefgui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import goal.preferences.ProfilerPreferences;
import goal.tools.profiler.InfoType;

/**
 * Provides a JPanel allowing modification of the Profiler preference settings.
 * We set the preferences through {@link goal.preferences.LoggingPreferences}.
 */
public class ProfilerPrefPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -3283487658286203531L;
	private final JCheckBox profiling = new JCheckBox("Profile agents");
	private final JCheckBox profileToFile = new JCheckBox("Profile to file (instead of console)");
	private final JCheckBox logNodeID = new JCheckBox("Log profile node IDs");

	/**
	 * create the panel that allows user to change the prefs.
	 */
	public ProfilerPrefPanel() {

		// setLayout(new GridLayout(0, 1));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(PreferencesPanel.getBoldFontJLabel("Profiling"));
		add(this.profiling);
		add(this.profileToFile);
		add(this.logNodeID);
		add(new JPopupMenu.Separator());

		add(PreferencesPanel.getBoldFontJLabel("Data to show"));

		this.profiling.addActionListener(this);
		this.profileToFile.addActionListener(this);
		this.logNodeID.addActionListener(this);

		for (InfoType type : InfoType.values()) {
			MyCheckBox checkbox = new MyCheckBox(type);
			add(checkbox);
		}

		initSettings();

	}

	/**
	 * Copies settings from preferences to the check boxes and spinner model.
	 */
	private void initSettings() {
		this.profiling.setSelected(ProfilerPreferences.getProfiling());
		this.profileToFile.setSelected(ProfilerPreferences.getProfilingToFile());
		this.logNodeID.setSelected(ProfilerPreferences.getLogNodeId());
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		ProfilerPreferences.setProfiling(this.profiling.isSelected());
		ProfilerPreferences.setProfilingToFile(this.profileToFile.isSelected());
		ProfilerPreferences.setLogNodeId(this.logNodeID.isSelected());
	}
}

/**
 * Checkbox for InfoType selection. Includes selection handler and initializer
 *
 */
class MyCheckBox extends JCheckBox implements ActionListener {
	private static final long serialVersionUID = 1301049493749442595L;
	private InfoType type;

	public MyCheckBox(InfoType type) {
		super(type.getDescription(), ProfilerPreferences.isTypeSelected(type));
		this.type = type;
		addActionListener(this);
	}

	public InfoType getType() {
		return this.type;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ProfilerPreferences.setTypeSelected(this.type, isSelected());
	}

}