/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tudelft.goal.SimpleIDE.filenodes;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.TreeNode;

import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * The root node for the file panel. Swing requires a root node always.
 *
 * @author W.Pasman 25aug15
 *
 */
public class FileRootNode extends AbstractFileNode {

	private final TreeNode mas, other;
	private ArrayList<TreeNode> list;

	public FileRootNode(IdeFiles files) {
		super(files, null, null);
		this.mas = new MasRootNode(files, this);
		this.other = new OtherRootNode(files, this);

		this.list = new ArrayList<>();
		this.list.add(this.mas);
		this.list.add(this.other);

	}

	@Override
	public List<TreeNode> getChildren() {
		return this.list;
	}

	@Override
	public String getName() {
		return "";
	}

}