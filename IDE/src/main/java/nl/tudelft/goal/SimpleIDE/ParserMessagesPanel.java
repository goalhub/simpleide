package nl.tudelft.goal.SimpleIDE;

import java.io.File;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import goal.util.Observer;
import languageTools.errors.Message;

/**
 * Shows all current parser errors
 *
 * @author W.Pasman
 *
 */
public class ParserMessagesPanel extends JScrollPane {
	private static final long serialVersionUID = -4639663680838109224L;
	JTextArea text = new JTextArea();
	IdeFiles fileInfo = IdeFiles.getInstance();

	public ParserMessagesPanel() {
		setViewportView(text);

		fileInfo.addObserver(new Observer<IdeFiles, File>() {
			@Override
			public void eventOccured(IdeFiles source, File evt) {
				refreshErrors();
			}
		});
	}

	/**
	 * refresh the displayed errors.
	 */
	private void refreshErrors() {
		String message = "";
		for (File file : fileInfo.getUsedFiles()) {
			Set<Message> errors = fileInfo.getErrors(file);
			Set<Message> warnings = fileInfo.getWarnings(file);
			if (errors.isEmpty() && warnings.isEmpty()) {
				continue;
			}
			message += "\n";
			for (Message error : errors) {
				message += error.toString() + "\n";
			}
			for (Message warning : warnings) {
				message += warning.toString() + "\n";
			}
		}
		text.setText(message);
	}
}
