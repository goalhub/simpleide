/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tudelft.goal.SimpleIDE.filenodes;

import java.io.File;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import languageTools.utils.Extension;
import nl.tudelft.goal.SimpleIDE.IconFactory;
import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * The root node for the mas files.
 *
 * @author W.Pasman 25aug15
 *
 */
public abstract class AbstractFileNode implements TreeNode {

	private IdeFiles files;
	private AbstractFileNode parent;
	private File basefile;

	/**
	 * 
	 * @param files
	 *            the {@link IdeFiles}
	 * @param parent
	 *            the parent node of this node.
	 * @param basefile
	 *            the file that this node is based on.Only non-leaf nodes can
	 *            return null. This should point to a hypothetical file (it does
	 *            not need to exist yet).
	 */
	public AbstractFileNode(IdeFiles files, AbstractFileNode parent, File basefile) {
		this.parent = parent;
		this.files = files;
		this.basefile = basefile;
	}

	/**
	 * @return the {@link IdeFiles}, these are all known files that we have.
	 */
	public IdeFiles getFiles() {
		return this.files;
	}

	/**
	 * You should implement this. If this returns null, the node can not have
	 * children. The rest is implemented here.
	 *
	 * @return list of the children of this node.
	 */
	public abstract List<TreeNode> getChildren();

	@Override
	public TreeNode getParent() {
		return this.parent;
	}

	/**
	 * Override this again if your node is not leaf.
	 */
	@Override
	public boolean isLeaf() {
		return false;
	}

	@Override
	public boolean getAllowsChildren() {
		return !isLeaf();
	}

	@Override
	public int getChildCount() {
		if (isLeaf()) {
			return 0;
		}
		return getChildren().size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return getChildren().indexOf(node);
	}

	@Override
	public Enumeration<TreeNode> children() {
		if (isLeaf()) {
			return null;
		}
		return Collections.enumeration(getChildren());
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		return getChildren().get(childIndex);
	}

	/**
	 * @return Icon for this node. Can return null if no basefile.
	 */
	public Icon getIcon() {
		if (this.basefile == null) {
			return null;
		}
		return getIconId().getIcon();
	}

	private IconFactory getIconId() {
		boolean existing = this.basefile.exists();
		Extension ext = Extension.getFileExtension(this.basefile);
		if (ext == null) {
			// unknown file
			return existing ? IconFactory.OTHER_FILE : IconFactory.NO_PL_FILE;
		}
		switch (Extension.getFileExtension(this.basefile)) {
		case MAS2G:
			return existing ? IconFactory.MAS_FILE : IconFactory.NO_MAS_FILE;
		case PL:
			return existing ? IconFactory.PL_FILE : IconFactory.NO_PL_FILE;
		case MOD2G:
			// FIXME change icon to mod file?
			return existing ? IconFactory.GOAL_FILE : IconFactory.NO_GOAL_FILE;
		// more icons?
		default:
			// FIXME create NO_OTHER_FILE.
			return existing ? IconFactory.OTHER_FILE : IconFactory.NO_PL_FILE;
		}
	}

	/**
	 * All DefaultNodes are based on a file
	 *
	 * @return the file that this node is based on. Only non-leaf nodes can
	 *         return null. This should point to a hypothetical file (it does
	 *         not need to exist yet).
	 */
	public File getBaseFile() {
		return this.basefile;
	}

	/**
	 * @return the {@link MASNode} that is the parent of this node. May also be
	 *         the node itself. Returns null if there is no parent
	 *         {@link MASNode}.
	 */
	public MASNode getParentMASNode() {
		if (this.parent == null) {
			return null;
		}
		return this.parent.getParentMASNode();
	}

	/**
	 * @return the name / label to show to the user for this node.
	 */
	abstract public String getName();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.basefile == null ? 0 : this.basefile.hashCode());
		return result;
	}

	/**
	 * Equals returns true iff the objects are same class and refer to the same
	 * file. We can't check the absolute tree position here of the nodes.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractFileNode other = (AbstractFileNode) obj;
		if (this.basefile == null) {
			if (other.basefile != null) {
				return false;
			}
		} else if (!this.basefile.equals(other.basefile)) {
			return false;
		}
		return true;
	}

	/**
	 * @return where we expect the file to be , including the expected
	 *         extension.. This is needed if user wants to create the file
	 */
	public File getExpectedFile() {

		File directory = basefile.getParentFile();
		if (directory == null && parent != null && parent.getBaseFile() != null) {
			directory = parent.getBaseFile().getParentFile();
		}

		String filename = basefile.getName();

		Extension extension = Extension.getFileExtension(basefile);
		// FIXME should we guess smarter?
		if (extension == null) {
			extension = Extension.MOD2G;
			filename = filename + "." + extension;
		}

		return new File(directory, filename);
	}

}