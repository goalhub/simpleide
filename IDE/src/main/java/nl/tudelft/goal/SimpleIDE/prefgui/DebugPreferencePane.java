/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.prefgui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import events.Channel;
import events.Channel.ChannelState;
import goal.preferences.DebugPreferences;

/**
 * Panel that shows the breakpoints that can be set and the type of debug
 * messages that can be shown while running a GOAL MAS.
 *
 * <p>
 * This functionality is needed also in uninstaller Therefore avoid general
 * dependencies on GOAL, e.g. don't use the Logger, don't use callbacks to the
 * SimpleIDE etc
 * </p>
 *
 * @author KH
 * @modified W.Pasman 13feb2012 do not use any GOAL classes here, they will not
 *           be included in the uninstaller. Removed the reference to GOALBug.
 * @modified V.Koeman 12jun13 refactoring preferences: management separated from
 *           display through new class
 * @modified W.Pasman 28may2014 fixed cyclic dependency of model on view.
 *           Separated view from model. model directly attached to
 *           {@link DebugPreferences}. Removed channelstate fiddling from the
 *           view.
 */
public class DebugPreferencePane extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = -2387796645759964908L;
	/**
	 * This is a list of all channels that the user can edit.
	 */
	private final List<Channel> channels = new ArrayList<>();
	private TableModelAdapter model = new TableModelAdapter();

	/**
	 * Provides a preference panel that allows user to set its preferences for
	 * viewing debug messages and for pausing on specific breakpoints.
	 *
	 * @param parent
	 *            the IDE that owns this panel, needed to handle update events
	 *            related to changes in viewing and pausing preferences.
	 */
	public DebugPreferencePane() {

		super(new BorderLayout());

		for (Channel channel : Channel.values()) {
			// do not display hidden channels
			if (!DebugPreferences.getChannelState(channel).isHidden()) {
				this.channels.add(channel);
			}
		}

		// Create display.
		JTable table = new JTable(model);
		table.setPreferredScrollableViewportSize(table.getPreferredScrollableViewportSize());
		table.setFillsViewportHeight(true);
		table.setCellSelectionEnabled(false);

		// Center headers.
		((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer())
				.setHorizontalAlignment(SwingConstants.CENTER);

		// Create the scroll pane and add the table to it
		JScrollPane scrollPane = new JScrollPane(table);

		// Initialize sizes
		initSize(table);

		// Add the scroll pane to this panel
		add(scrollPane, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel(new BorderLayout());
		buttonPanel.add(allNoneButton(), BorderLayout.WEST);
		buttonPanel.add(resetButton(), BorderLayout.EAST);

		add(buttonPanel, BorderLayout.SOUTH);
	}

	private JButton resetButton() {
		JButton resetButton = new JButton("Reset to default");
		resetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DebugPreferences.reset();
			}
		});
		return resetButton;
	}

	private JButton allNoneButton() {
		JButton allButton = new JButton("Select All View");
		allButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				model.setAllView();
			}
		});
		return allButton;
	}

	/**
	 * Determines good column sizes and table height.
	 */
	private void initSize(JTable table) {
		TableColumn column = null;
		Component comp = null;

		int tableWidth = 0;
		for (int i = 0; i < 3; i++) {
			column = table.getColumnModel().getColumn(i);

			comp = table.getDefaultRenderer(String.class).getTableCellRendererComponent(table,
					model.getColumnNames()[i], false, false, 0, i);

			column.setPreferredWidth(comp.getPreferredSize().width);
			tableWidth += Math.round(comp.getPreferredSize().width * 1.5);
		}

		// adjust height to number of rows in table
		int height = table.getRowCount() * table.getRowHeight() + table.getRowHeight();
		table.setPreferredScrollableViewportSize(new Dimension(tableWidth, height));
	}

}

/**
 * Direct adapter to the DebugPreferences. Thread safe. immutable.
 *
 */
class TableModelAdapter implements TableModel {

	private final List<Channel> channels = new ArrayList<>();
	// columnNames should be long enough to accomodate the longest value for
	// that column. So do not remove extra whitespace paddings.
	private final String[] columnNames = { "Breakpoint                        ", "View", "Pause" };
	private final List<TableModelListener> listeners = new CopyOnWriteArrayList<>();

	public TableModelAdapter() {
		for (Channel channel : Channel.values()) {
			// do not display hidden channels
			if (!DebugPreferences.getChannelState(channel).isHidden()) {
				this.channels.add(channel);
			}
		}

		// directly hook up the changes
		DebugPreferences.addChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				TableModelEvent changeEvent = new TableModelEvent(TableModelAdapter.this);
				for (TableModelListener l : listeners) {
					l.tableChanged(changeEvent);
				}
			}
		});
	}

	@Override
	public int getRowCount() {
		return channels.size();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return String.class;
		case 1:
		case 2:
			return Boolean.class;
		}
		throw new IllegalArgumentException("illegal column " + columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Channel channel = channels.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return channel.getExplanation();
		case 1:
			return DebugPreferences.getChannelState(channel).canView();
		case 2:
			return DebugPreferences.getChannelState(channel).shouldPause();
		}
		throw new IllegalArgumentException("illegal column " + columnIndex);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if (columnIndex < 1 || columnIndex > 3)
			throw new IllegalArgumentException("illegal column " + columnIndex);

		Boolean newValue = (Boolean) aValue;
		Channel channel = channels.get(rowIndex);
		ChannelState newState = null, current = DebugPreferences.getChannelState(channel);
		switch (columnIndex) {
		case 1:
			// if we turn ON view, pause must have been off.
			// if view was on, pause must be OFF afterwards.
			newState = computeState(newValue, false);
			break;
		case 2:
			newState = computeState(current.canView(), newValue);
			break;
		}
		DebugPreferences.setChannelState(channel, newState);

	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
	}

	/**
	 * Compute the channel state value from a view and a pause value
	 *
	 * @param view
	 *            Channel.VIEW true or false. If pause is set to true, view is
	 *            overridden to true.
	 * @param pause
	 *            Channel.PAUSE true or false.
	 */
	private ChannelState computeState(Boolean view, Boolean pause) {
		if (pause) {
			return ChannelState.VIEWPAUSE; // PAUSE also enables VIEW
		}
		return view ? ChannelState.VIEW : ChannelState.NONE;
	}

	/**
	 * @return Strings that match longest string for each column. Used for
	 *         determining the column widths.
	 */
	public String[] getColumnNames() {

		return columnNames;
	}

	/**
	 * Set all channels to view
	 */
	public void setAllView() {
		for (Channel channel : channels) {
			ChannelState newState = computeState(true, DebugPreferences.getChannelState(channel).shouldPause());
			DebugPreferences.setChannelState(channel, newState);
		}
	}

}
