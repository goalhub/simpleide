/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.tree.TreeNode;

import goal.preferences.CorePreferences;
import goal.tools.errorhandling.exceptions.GOALUserError;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IconFactory;
import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * Create new file. What kind of file depends on the selection in file panel.
 *
 * @author W.Pasman 20jun2011
 */
public class OpenFileAction extends GOALAction {
	private static final long serialVersionUID = 3384899933517250201L;

	public OpenFileAction() {
		setIcon(IconFactory.OPEN_FILE.getIcon());
		setShortcut('O');
		setDescription("Open existing file"); //$NON-NLS-1$
	}

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		setActionEnabled(currentState.getViewMode() == IDEMainPanel.EDIT_VIEW);
	}

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent e) throws GOALUserError {
		File theFile;

		theFile = FileUtilities.askFile(getIDE().getMainPanel(), true, "Open file", JFileChooser.FILES_ONLY, null, null, //$NON-NLS-1$
				CorePreferences.getAgentBrowsePath(), true);
		if (theFile != null) {
			if (CorePreferences.getRememberLastUsedAgentDir()) {
				CorePreferences.setAgentBrowsePath(theFile.getParent());
			}
			// FIXME should we CREATE the file or just add it?
			IdeFiles.getInstance().add(theFile);
		}
	}

}
