/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.filenodes;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * Custom renderer, to add our custom status icons to process nodes.
 */
public class NodeRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 6680711649402774636L;

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value1, boolean sel, boolean expanded,
			boolean leaf, int row, boolean hasFocus) {
		AbstractFileNode value = (AbstractFileNode) value1;
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		setIcon(value.getIcon());
		setText(value.getName());

		return this;
	}
}
