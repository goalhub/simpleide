package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.Component;
import java.awt.Container;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import goal.preferences.CorePreferences;
import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import languageTools.utils.Extension;
import nl.tudelft.goal.SimpleIDE.EditManager;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * Utilities to support actions
 *
 * @author W.Pasman 27aug15
 *
 */
public class FileUtilities {

	private static final String TEMPLATE = "nl/tudelft/goal/SimpleIDE/files/template";

	/**
	 * Opens file browser panel and asks user to select a filename/directory.
	 *
	 * @param parentpanel
	 *            the panel to center this file browser panel on.
	 * @param openFile
	 *            boolean that must be set to true to open a file dialog, and to
	 *            false to open a save dialog.
	 * @param title
	 *            the title for the file browser panel.
	 * @param mode
	 *            The type of files to be displayed:
	 *            <ul>
	 *            <li>JFileChooser.FILES_ONLY
	 *            <li>JFileChooser.DIRECTORIES_ONLY
	 *            <li>JFileChooser.FILES_AND_DIRECTORIES
	 *            </ul>
	 *
	 * @param ext
	 *            the expected extension. If the extension is not starting with
	 *            a dot, we insert a dot. Is enforced (if not null) by simply
	 *            adding when user enters a filename without any extension, or
	 *            by requesting user to change it if it is not right. If
	 *            enforceExtension = true, we throw exception when user changes
	 *            extension. If false, we allow explicit different extension by
	 *            user.
	 * @param defaultName
	 *            File name that will be suggested (if not null) by the dialog.
	 *            Should be of the form defaultName###.extension. Note that if
	 *            openFile is set to true then an existing label is picked of
	 *            that form, and if openFile is set to false then a new name is
	 *            picked.
	 * @param startdir
	 *            is the suggested start location for the browse. If this is set
	 *            to null, {@link System#getProperty(String)} is used with
	 *            String="user.home".
	 * @param enforceExtension
	 *            is set to true if extension MUST be used. false if extension
	 *            is only suggestion and can be changed (to a known
	 *            {@link Extension}). Only applicable if extension is not null.
	 * @return The selected file, or null if user cancels the dialog. Adds
	 *         default extension if file path does not have file extension.
	 * @throws GOALCommandCancelledException
	 *             if user cancels action.
	 * @throws GOALUserError
	 *             if user makes incorrect selection.
	 *
	 */
	public static File askFile(Component parentpanel, boolean openFile, String title, int mode, String exten,
			String defaultName, String startdir, boolean enforceExtension) throws GOALUserError {
		// insert the leading dot if necessary.
		String extension = exten == null || exten.startsWith(".") ? exten //$NON-NLS-1$
				: "." + exten; //$NON-NLS-1$
		/**
		 * File name to be returned.
		 */
		File selectedFile;
		/**
		 * Return state of file chooser.
		 */
		int returnState;

		if (startdir == null) {
			startdir = System.getProperty("user.home"); //$NON-NLS-1$
		}

		try {
			File dir = new File(startdir);
			String suggestion = null;
			if (defaultName != null) {
				if (openFile) {
					suggestion = findExistingFilename(dir, defaultName, extension);
				} else {
					suggestion = createNewNonExistingFilename(dir, defaultName, extension);
				}
			}
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(dir);
			chooser.setDialogTitle(title);
			chooser.setFileSelectionMode(mode);
			if (suggestion != null) {
				chooser.setSelectedFile(new File(suggestion));
			}

			if (openFile) {
				returnState = chooser.showOpenDialog(parentpanel);
			} else {
				returnState = chooser.showSaveDialog(parentpanel);
			}

			// TODO: handle ALL return state options + exception here
			if (returnState != JFileChooser.APPROVE_OPTION) {
				return null;
			}

			selectedFile = chooser.getSelectedFile();
			if (openFile && !selectedFile.exists()) {
				// file browsers you can select
				// non-existing files!
				throw new FileNotFoundException("File " //$NON-NLS-1$
						+ selectedFile.getCanonicalPath() + " does not exist."); //$NON-NLS-1$
			}
		} catch (FileNotFoundException e) {
			throw new GOALUserError(e.getMessage(), e);
		} catch (IOException e) {
			throw new GOALUserError(e.getMessage(), e);
		} catch (HeadlessException e) {
			throw new GOALUserError(e.getMessage(), e);
		}

		// Check extension.
		String ext = FilenameUtils.getExtension(selectedFile.getName());
		if (extension != null) {
			if (ext.isEmpty()) {
				selectedFile = new File(selectedFile.getAbsolutePath() + extension);
			} else {
				if (Extension.getFileExtension(selectedFile) == null) {
					throw new GOALUserError(
							"The file must have a known extension: " + Arrays.toString(Extension.values()));
				}
				// Check whether extension is OK.
				if (enforceExtension && !("." + ext).equals(extension)) { //$NON-NLS-1$
					throw new GOALUserError("The file must have extension " //$NON-NLS-1$
							+ extension);
				}
			}
		}
		Extension fileExtension = Extension.getFileExtension(selectedFile.getName());
		if (fileExtension == null) {
			throw new GOALUserError("Files with extension " + ext //$NON-NLS-1$
					+ " are not recognized by GOAL"); //$NON-NLS-1$
		}

		// Check whether file name already exists.
		if (!openFile && selectedFile != null && selectedFile.exists()) {
			int selection = JOptionPane.showConfirmDialog(parentpanel, "Overwrite existing file?", "Overwrite?", //$NON-NLS-1$ //$NON-NLS-2$
					JOptionPane.YES_NO_OPTION);
			if (selection != JOptionPane.YES_OPTION) {
				return null; // User refused overwrite of file
			}
		}

		return selectedFile;
	}

	/**
	 * TRAC #700
	 *
	 * @return a filename in given directory that starts with name and has
	 *         extension, or {@code null} if no such file was found. If dir is a
	 *         file instead of path, the directory containing dir is used.
	 *
	 *         TODO: should be reimplemented using, e.g., listFiles(File
	 *         directory, IOFileFilter fileFilter, IOFileFilter dirFilter) from
	 *         Apache Commons IO.
	 */
	private static String findExistingFilename(File dir, String name, String ext) {
		String[] children;

		if (dir.isFile()) {
			dir = dir.getParentFile();
		}
		children = dir.list();
		if (children == null) {
			return null;
		}
		for (String child : children) {
			if (child.startsWith(name) && child.endsWith(ext)) {
				return child;
			}
		}
		return null;
	}

	/**
	 * TRAC #700
	 *
	 * @return a filename that starts with name and has extension, and does not
	 *         exist in given directory. If dir is a file instead of path, the
	 *         directory containing dir is used.
	 */
	private static String createNewNonExistingFilename(File dir, String name, String ext) {
		String[] chs;
		String newname;

		if (dir.isFile()) {
			dir = dir.getParentFile();
		}
		chs = dir.list();
		if (chs == null) {
			return name + ext;
		}
		ArrayList<String> children = new ArrayList<>(Arrays.asList(chs));
		// Check if label + ext does not yet exist
		if (!children.contains(name + ext)) {
			return name + ext;
		} else { // Search for a number that, when added to the file name,
			// yields
			// a non-existing file name
			int n = 1;
			do {
				newname = name + n + ext;
				n++; // assumes less than 2,147,483,647 file names of form
				// "name + n + ext"
			} while (children.contains(newname));
			return newname;
		}
	}

	/**
	 * Ask user if it's ok to delete a file, and then delete it.
	 *
	 * @param file
	 *            {@link File} to delete. This file MUST {@link File#exists()}.
	 * @param files
	 *            the {@link IdeFiles}
	 * @param comp
	 *            the component to show dialogs on
	 * @return true if file was deleted, else false.
	 */
	public static boolean checkAndDelete(File file, IdeFiles files, Component comp) {
		if (!confirmDelete(file, comp)) {
			return false;
		}
		delete(file, files);
		return true;
	}

	/**
	 * Delete a file. Also handles including possibly open editors and updating
	 * the known-files
	 *
	 * @param file
	 *            {@link File} to delete
	 * @param currentState
	 *            current {@link IDEState}
	 */
	private static void delete(File file, IdeFiles files) {
		try {
			EditManager.getInstance().close(file);
		} catch (GOALException e) {
			e.printStackTrace();
		}

		file.delete();
		files.remove(file);
	}

	/**
	 * Ask user to confirm delete of a file
	 *
	 * @param file
	 *            {@link File} to delete
	 * @param comp
	 *            the {@link Component} to center the dialog on.
	 * @return true iff user confirmed the delete
	 */
	public static boolean confirmDelete(File file, Component comp) {
		int selection = JOptionPane.showConfirmDialog(comp,
				"This will delete the file " + file //$NON-NLS-1$
						+ " from the file system. Please confirm.", //$NON-NLS-1$
				"Pleas Confirm", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$
		return selection == JOptionPane.YES_OPTION;
	}

	/**
	 * create new file, insert in the file browser, and open it for editing.
	 *
	 * @param file
	 *            the {@link File} to add
	 * @param fileReg
	 *            the {@link IdeFiles}
	 * @throws IOException
	 *             if file could not be created.
	 */
	public static void createFileAndEdit(File file, IdeFiles fileReg) throws IOException {
		createFile(file, fileReg);
		EditManager.getInstance().editFile(file);
	}

	/**
	 * Create template file and show it in the user's file panel. The new
	 * created file must have a known {@link Extension}.
	 *
	 * @param newFile
	 *            the {@link File} to be created. File MUST have a known
	 *            extension or this will throw NPE.
	 * @throws IOException
	 */
	// FIXME move this somewhere else.
	public static void createFile(File newFile, IdeFiles fileReg) throws IOException {

		Extension extension = Extension.getFileExtension(newFile);
		InputStream template = IdeFiles.class.getClassLoader()
				.getResourceAsStream(TEMPLATE + "." + extension.getExtension());
		createAndInitFile(newFile, fileReg, template);
	}

	/**
	 * create and load a new file with given contents, and inserts the new file
	 * into the file registry.
	 *
	 * @param newFile
	 *            the file to create
	 * @param fileReg
	 *            the {@link IdeFiles}
	 * @param newFileContents
	 *            the {@link InputStream} with the new file contents.
	 * @throws IOException
	 */
	public static void createAndInitFile(File newFile, IdeFiles fileReg, InputStream newFileContents)
			throws IOException {
		FileOutputStream os = new FileOutputStream(newFile);
		IOUtils.copy(newFileContents, os);
		newFileContents.close();
		os.close();
		fileReg.add(newFile);
	}

	/**
	 * Renames a file to a new name. THe new name will be asked from the user.
	 * If the selected target file exists we overwrite the existing file with
	 * the given (after user's confirmation).
	 *
	 * @param oldFile
	 *            is the file to be renamed.
	 * @param parentPanel
	 *            the panel to center dialogs on.
	 *
	 * @param fileReg
	 *            the {@link IdeFiles}
	 * @throws GOALException
	 * @throws ParserException
	 */
	public static void askAndRename(File oldFile, Component parentPanel, IdeFiles fileReg) throws GOALException {
		String extension = FilenameUtils.getExtension(oldFile.getName());
		String oldfilename = FilenameUtils.removeExtension(oldFile.getAbsolutePath());
		File newFile = null;
		newFile = askFile(parentPanel, false, "Save as", JFileChooser.FILES_ONLY, extension, oldfilename,
				CorePreferences.getAgentBrowsePath(), true);
		if (newFile == null) {
			return; // user cancelled
		}
		rename(oldFile, newFile, parentPanel, fileReg);
	}

	/**
	 * move old file to new file. Old file is deleted, new file created. Old
	 * file removed from IdeFile, new file added. If old file was open in
	 * editor, we re-open new file in editor after the rename.
	 *
	 * @param oldFile
	 *            is the file to be renamed.
	 * @param newFile
	 *            the new file. If this file already exists, it will be
	 *            overwritten.
	 * @param parentPanel
	 *            the panel to center dialogs on.
	 * @param fileReg
	 *            the {@link IdeFiles}
	 * @throws GOALException
	 */
	public static void rename(File oldFile, File newFile, Component parentPanel, IdeFiles fileReg)
			throws GOALException {
		if (CorePreferences.getRememberLastUsedAgentDir()) {
			CorePreferences.setAgentBrowsePath(newFile.getParent());
		}
		boolean reopen = EditManager.getInstance().isOpenEditor(oldFile);
		EditManager.getInstance().close(oldFile);
		EditManager.getInstance().close(newFile);

		try {
			createAndInitFile(newFile, fileReg, new FileInputStream(oldFile));
		} catch (IOException e) {
			throw new GOALUserError("Cannot copy " + oldFile + " to " + newFile, e);
		}

		oldFile.delete();
		if (reopen) {
			EditManager.getInstance().editFile(newFile);
		}
		delete(oldFile, fileReg);
	}

	/**
	 *
	 * propose user to create new file as it does not exist now. Throws
	 * GOALUserError if user cancels the proposal or if creation of file failed.
	 *
	 * It's a bit weird to have static function in FilePanel, but this seems the
	 * best place for this utility function.
	 *
	 * @param parent
	 *            is the GUI parent for this panel, used for centering the
	 *            panel.
	 * @param newFile
	 *            is {@link File} to be created. Filename should have the
	 *            extension.
	 */
	public static void proposeToCreate(Container parent, File newFile, IdeFiles files) throws GOALUserError {
		int selection = JOptionPane.showConfirmDialog(parent,
				"The file " //$NON-NLS-1$
						+ newFile + " does not exist. Create it?", //$NON-NLS-1$
				"Create new file?", JOptionPane.YES_NO_OPTION); //$NON-NLS-1$
		if (selection == JOptionPane.NO_OPTION) {
			throw new GOALUserError("File " + newFile //$NON-NLS-1$
					+ " does not exist; creation cancelled"); //$NON-NLS-1$
		}
		try {
			FileUtilities.createFile(newFile, files);
		} catch (IOException e) {
			throw new GOALUserError("Cannot create file " + newFile, e); //$NON-NLS-1$
		}
	}

}
