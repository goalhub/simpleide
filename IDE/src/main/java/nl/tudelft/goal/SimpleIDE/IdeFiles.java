package nl.tudelft.goal.SimpleIDE;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import goal.tools.errorhandling.Warning;
import goal.util.Observable;
import goal.util.Observer;
import languageTools.Analyzer;
import languageTools.analyzer.FileRegistry;
import languageTools.analyzer.Validator;
import languageTools.analyzer.mas.MASValidator;
import languageTools.errors.Message;
import languageTools.program.Program;
import languageTools.program.mas.MASProgram;
import languageTools.program.mas.UseClause;

/**
 * This contains the files currently shown in the user's file panel, including
 * parsed {@link Program}s and the parse errors that occured in the files.
 *
 * This interface also offers to sort them as (1) MAS programs (2) other files.
 *
 * @author W.Pasman 24aug15
 *
 */
public class IdeFiles implements Observable<Observer<IdeFiles, File>, IdeFiles, File> {

	private static IdeFiles instance;

	private IdeFiles() {
	};

	public static IdeFiles getInstance() {
		if (instance == null) {
			instance = new IdeFiles();
		}
		return instance;
	}

	/**
	 * A recent instance, this is reset every time{@link #updateAllFiles} is called.
	 */
	private FileRegistry fileRegistry = new FileRegistry();

	/**
	 * our observers.
	 */
	private Set<Observer<IdeFiles, File>> myObservers = new HashSet<>();

	/**
	 * tricky. We have problems with parallelism where the GUI is trying to show all
	 * MASs while some other thread is adding new MASs. Not clear if
	 * ConcurrentHashMap is really suited to handle this. But in practice this seems
	 * to work fine.
	 */
	private Map<File, ParseResult> programs = new ConcurrentHashMap<>();

	/**
	 * Try to parse a file and find its program.
	 *
	 * @param file the file to be parsed.
	 * @return {@link Program} from the parsed file, or null if the file can not be
	 *         parsed (eg it does not exist or is unknown type).
	 */
	public Program getProgram(File file) {
		changedFile(file);
		return this.programs.get(file).getProgram();
	}

	/**
	 * If the file has changed, reparse the MAS(ses) that contain given file
	 *
	 * @param file the file that may have changed.
	 */
	public void notifyChange(File file) {
		changedFile(file);
	}

	/**
	 * Create entry in {@link #programs} if it's not there yet and update all files.
	 *
	 * @param file a file that changed, or may be just created.
	 */
	private void changedFile(File file) {
		createEntry(file);

		updateAllMAS();
	}

	/**
	 * Create entry in {@link #programs} if it's not there yet
	 *
	 * @param file the new file
	 */
	private void createEntry(File file) {
		if (file.exists() && !this.programs.containsKey(file)) {
			this.programs.put(file, new ParseResult(file));
			updateAllMAS();
		}
	}

	/**
	 *
	 * @param file
	 * @return errors we had when parsing/procesing a given file. Empty set if the
	 *         file was not parsed at all
	 */
	public Set<Message> getErrors(File file) {
		if (get(file) == null) {
			return new HashSet<>();
		}
		return get(file).getErrors();
	}

	/**
	 *
	 * @param file
	 * @return warnings we had when parsing/procesing a given file.Empty set if the
	 *         file was not parsed at all
	 */
	public Set<Message> getWarnings(File file) {
		if (get(file) == null) {
			return new HashSet<>();
		}

		return get(file).getWarnings();
	}

	/**
	 * Get the current list of MAS files. The order of this list is unspecified
	 *
	 * @return list of MAS programs
	 */
	public List<File> getMasPrograms() {

		List<File> list = new ArrayList<>();

		for (ParseResult prog : this.programs.values()) {
			if (prog.isMas()) {
				list.add(prog.getFile());
			}
		}
		return list;
	}

	/**
	 * @return a list of all MAS files and the files used by them (recursively)
	 */
	public List<File> getUsedFiles() {

		List<File> files = new ArrayList<>();

		for (ParseResult prog : this.programs.values()) {
			if (prog.isMas()) {
				files.add(prog.getFile());
				files.addAll(prog.getUsedFiles());
			}
		}
		return files;
	}

	/**
	 *
	 * @return list of all known files that are NOT currently used in a MAS.The
	 *         order of this list is unspecified
	 */
	public List<File> getOtherFiles() {
		List<File> list = new ArrayList<>(this.programs.keySet());
		list.removeAll(getUsedFiles());
		return list;
	}

	/**
	 * Remove a file and all the sub-files that it refers to.
	 *
	 * @param oldFile
	 */
	public void remove(File oldFile) {

		if (this.programs.containsKey(oldFile)) {
			Set<File> toRemove = this.programs.get(oldFile).getUsedFiles();
			toRemove.add(oldFile);

			for (File file : toRemove) {
				this.programs.remove(file);
			}
		}
		notifyObservers(this, oldFile);
	}

	/**
	 * Add an already existing file to the known files. If this is file of known
	 * type, the file is also parsed.
	 *
	 *
	 * @param newFile
	 */
	public void add(File newFile) {
		changedFile(newFile);
	}

	/**
	 * @return the {@link FileRegistry} of the IDE.
	 */
	public FileRegistry getFileRegistry() {
		return this.fileRegistry;
	}

	/**
	 * @param masFile
	 * @return all the files that the given mas refers to. Should include also the
	 *         lowest level .pl files
	 */
	public Set<File> getUsedFiles(MASProgram program) {
		return get(program.getSourceFile()).getUsedFiles();
	}

	/**************** implements Observable **********************/
	@Override
	public void addObserver(Observer<IdeFiles, File> observer) {
		synchronized (this.myObservers) {
			this.myObservers.add(observer);
		}
	}

	@Override
	public void removeObserver(Observer<IdeFiles, File> observer) {
		synchronized (this.myObservers) {
			this.myObservers.remove(observer);
		}
	}

	// FIXME this should not be public
	@Override
	public void notifyObservers(IdeFiles src, File changedFile) {
		synchronized (this.myObservers) {
			for (Observer<IdeFiles, File> observer : this.myObservers) {
				try {
					observer.eventOccured(src, changedFile);
				} catch (RuntimeException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/****************** Support functions *******************/
	/**
	 * Update all known files, but only if any known file has changed. It then also
	 * updates the {@link #fileRegistry}.
	 */
	private synchronized void updateAllMAS() {
		// parsing only works ok if we provide new empty FileRegistry.
		this.fileRegistry = new FileRegistry();
		boolean updated = false;
		for (ParseResult res : new ArrayList<>(this.programs.values())) {
			updated |= updateMAS(res);
		}

		if (updated) {
			notifyObservers(this, null);
		}
	}

	/**
	 * re-parse a mas file and copy all results from the registry back. Does nothing
	 * if parse is not a MAS. Does nothing if none of the files relevant for this
	 * MAS have changed.
	 *
	 * @param parse parse result that is already in {@link #programs}
	 * @return true iff new parsing has been done.
	 */
	private boolean updateMAS(ParseResult parse) {
		File file = parse.getFile();
		if (!file.exists() || !parse.isMas()) {
			return false;
		}

		if (parse.getUsedFiles() == null || !fileChanged(parse.getUsedFiles())) {
			return false;
		}

		try {
			Validator<?, ?, ?, ?> validator = Analyzer.processFile(file, this.fileRegistry);
			if (!(validator instanceof MASValidator)) {
				throw new IllegalArgumentException("file is not a mas");
			}
			((MASValidator) validator).process();

			syncWithFileRegistry(this.fileRegistry.getAllErrors(), this.fileRegistry.getWarnings());
		} catch (IOException e) {
			throw new IllegalArgumentException(e.getMessage());
		}

		/*
		 * the parse process may not have reached all used files. We must set all used
		 * files to checked anyway, to avoid repeating the update again and again.
		 */
		setChecked(parse);
		return true;
	}

	private void setChecked(ParseResult parse) {
		for (File f : parse.getUsedFiles()) {
			if (get(f) != null && get(f).isOutdated()) {
				this.programs.put(f, get(f).checkedCopy());
			}
		}
	}

	/**
	 * Create entry for file
	 *
	 * @param file
	 * @return last parse result for file, or null if file failed to parse.
	 */
	private ParseResult get(File file) {
		createEntry(file);
		return this.programs.get(file);
	}

	/**
	 *
	 * @param usedFiles list of files to check
	 * @return true if any of the given files has changed
	 */
	private boolean fileChanged(Set<File> usedFiles) {
		for (File file : usedFiles) {
			if (get(file) != null && get(file).isOutdated()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * copy current {@link FileRegistry} results into {@link #programs}.
	 */
	private void syncWithFileRegistry(SortedSet<Message> errors, SortedSet<Message> warnings) {
		for (File f : this.fileRegistry.getSourceFiles()) {
			createEntry(f);
			Program program = this.fileRegistry.getProgram(f);
			this.programs.put(f,
					new ParseResult(f, program, filter(errors, f), filter(warnings, f), usedFiles(program)));
		}
	}

	/**
	 * @param errors
	 * @param f
	 * @return all {@link Message}s that relate to file f
	 */
	private Set<Message> filter(SortedSet<Message> errors, File f) {
		SortedSet<Message> filtered = new TreeSet<>();
		for (Message m : errors) {
			if (m == null || m.getSource() == null || m.getSource().getSource() == null) {
				// this is probably a bug but it's not worth crashing on this
				// one... SI-93
				new Warning("Error: no source available for error message " + m).emit();
				continue;
			}
			if (new File(m.getSource().getSource()).equals(f)) {
				filtered.add(m);
			}
		}
		return filtered;
	}

	/**
	 * Recursively walks through all files that this program uses, and collects all
	 * used files. It uses the current {@link FileRegistry} to collect the info
	 *
	 * @param program
	 * @return all used files
	 */
	private Set<File> usedFiles(Program program) {
		Set<File> list = new HashSet<>();
		if (program instanceof MASProgram) {
			MASProgram masprog = (MASProgram) program;
			if (masprog.getEnvironmentfile() != null) {
				list.add(masprog.getEnvironmentfile());
			}
			// MASProgram ignores useClauses
			for (String name : masprog.getAgentNames()) {
				getUsedFilesRecursive(masprog.getAgentDefinition(name), list);
			}

		} else {
			getUsedFilesRecursive(program, list);
		}
		return list;

	}

	private void getUsedFilesRecursive(Program program, Set<File> files) {
		if (program != null) {
			for (UseClause use : program.getUseClauses()) {
				getUsedFilesRecursive(use, files);
			}
		}
	}

	private void getUsedFilesRecursive(UseClause use, Set<File> files) {
		List<URI> refs = use.getResolvedUriReference();
		if (refs.isEmpty()) {
			// return a 'hacked' file. The file does not exist.
			// The user can decide to create it or to fix it.
			File file = new File(use.getReference());
			files.add(file);
			Program prog = this.fileRegistry.getProgram(file);
			if (prog != null) {
				getUsedFilesRecursive(prog, files);
			}
		} else {
			for (final URI ref : refs) {
				File file = new File(ref.getPath());
				files.add(file);
				Program prog = this.fileRegistry.getProgram(file);
				if (prog != null) {
					getUsedFilesRecursive(prog, files);
				}
			}
		}
	}
}
