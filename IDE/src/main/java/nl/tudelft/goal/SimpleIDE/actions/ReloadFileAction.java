/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.Resources;
import goal.tools.errorhandling.Warning;
import goal.tools.errorhandling.WarningStrings;
import goal.tools.errorhandling.exceptions.GOALBug;
import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import nl.tudelft.goal.SimpleIDE.EditManager;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;
import nl.tudelft.goal.SimpleIDE.filenodes.MASNode;
import nl.tudelft.goal.SimpleIDE.filenodes.SimpleFileNode;

/**
 * Create new file. What kind of file depends on the selection in file panel.
 *
 * @author W.Pasman 20jun2011
 */
public class ReloadFileAction extends GOALAction {
	private static final long serialVersionUID = 3235165005082859025L;

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		if (currentState.getViewMode() == IDEMainPanel.DEBUG_VIEW) {
			// We're in debug view. Process nodes can't be saved.
			setActionEnabled(false);
		} else {
			// We're in edit view.
			List<? extends AbstractFileNode> selection = currentState.getSelectedFileNodes();
			if (selection.isEmpty()) {
				setActionEnabled(false);
				return;
			}
			AbstractFileNode node = selection.get(0);
			setActionEnabled((node instanceof MASNode || node instanceof SimpleFileNode)
					&& EditManager.getInstance().isOpenEditor(node.getBaseFile()));
		}
	}

	@Override
	/**
	 * We override the default because we want to show a one-time requester
	 * whether user is sure to reload.
	 */
	public void executeAll(ActionEvent e) throws GOALException {
		List<? extends AbstractFileNode> sel = getCurrentState().getSelectedFileNodes();
		if (sel.isEmpty()) {
			throw new GOALBug("Reload should not be enabled if nothing is selected"); //$NON-NLS-1$
		}
		// CANCEL = NO in this case.
		int choice = JOptionPane.showConfirmDialog(getCurrentState().getRootComponent(),
				"Do you want to reload the selected files? All unsaved changes will be lost.", //$NON-NLS-1$
				"Reload?", JOptionPane.YES_NO_CANCEL_OPTION); //$NON-NLS-1$

		if (choice != JOptionPane.YES_OPTION) {
			return;
		}
		super.executeAll(e);
	}

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent e) {
		if (selectedNode instanceof MASNode || selectedNode instanceof SimpleFileNode) {
			AbstractFileNode fileNode = (AbstractFileNode) selectedNode;

			if (EditManager.getInstance().isOpenEditor(fileNode.getBaseFile())) {
				try {
					try {
						EditManager.getInstance().getEditor(fileNode.getBaseFile()).reload();
					} catch (GOALUserError e1) {
						new Warning(e1.getMessage(), e1).emit();
					}
				} catch (IOException er) {
					new Warning(String.format(Resources.get(WarningStrings.FAILED_FILE_RELOAD), fileNode.getBaseFile()),
							er).emit();
				}
			}
		}
	}

}
