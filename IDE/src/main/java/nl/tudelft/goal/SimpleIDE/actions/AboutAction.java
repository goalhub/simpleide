/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;

import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.exceptions.GOALException;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.menu.AboutBox;

/**
 * show the About box.
 *
 * @author W.Pasman 20jun2011
 */
public class AboutAction extends GOALAction {
	private static final long serialVersionUID = -363791847357203163L;

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent ae) throws GOALException {
		new AboutBox(getCurrentState().getRootComponent());
	}

	@Override
	public void eventOccured(IDEState source, Object evt) {
		setActionEnabled(true);
	}

}
