/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.filenodes;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.tree.TreeNode;

import languageTools.program.mas.MASProgram;
import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * node representing a MAS file.
 */
public class MASNode extends AbstractFileNode {

	public MASNode(IdeFiles files, AbstractFileNode parent, File basefile) {
		super(files, parent, basefile);
		if (basefile == null) {
			throw new NullPointerException("basefile is null");
		}
	}

	@Override
	public List<TreeNode> getChildren() {
		List<TreeNode> list = new ArrayList<>();

		MASProgram baseprog = (MASProgram) getFiles().getProgram(getBaseFile());
		if (baseprog != null) {
			Set<File> childs = IdeFiles.getInstance().getUsedFiles(baseprog);
			for (File f : childs) {
				list.add(new SimpleFileNode(getFiles(), this, f));
			}
		}

		return list;
	}

	@Override
	public MASNode getParentMASNode() {
		return this;
	}

	@Override
	public String getName() {
		return getBaseFile().getName();
	}

}
