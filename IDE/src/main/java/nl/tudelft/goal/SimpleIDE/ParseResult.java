package nl.tudelft.goal.SimpleIDE;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import languageTools.errors.Message;
import languageTools.program.Program;
import languageTools.utils.Extension;

/**
 * Stores parse results.
 *
 * @author W.Pasman
 *
 */
public class ParseResult {
	private File file;
	private Long lastParseTime = 0l;
	private Set<Message> errors = new HashSet<>();
	private Set<Message> warnings = new HashSet<>();
	private Program program = null;
	private Set<File> usedFiles = new HashSet<>();

	/**
	 * Default (file not parsed) result. Leaves lastParseTime at 0 indicating
	 * the file was not even parsed.
	 *
	 * @param file
	 *            must not be null. Can be non-existing file.
	 */
	public ParseResult(File file) {
		if (file == null) {
			throw new NullPointerException("file");
		}
		this.file = file;
		this.usedFiles.add(file);
	}

	/**
	 * Creates a copy of this ParseResult, but with the update time set to the
	 * current version of the file.
	 *
	 * @return
	 */
	public ParseResult checkedCopy() {
		return new ParseResult(this.file, this.program, this.errors, this.warnings, this.usedFiles);
	}

	/**
	 *
	 * @return true iff this Parse result is outdated because the file is newer
	 *         than the time we updated this
	 */
	public boolean isOutdated() {
		return this.file.exists() && this.file.lastModified() > this.lastParseTime;
	}

	/**
	 *
	 * @param file
	 *            must not be null. Can be non-existing file.
	 * @param program
	 *            parsed program. Can be null
	 * @param errors
	 *            . Can be null (indicates empty list)
	 * @param warnings
	 *            can be null (indicates empty list)
	 * @param usedfiles
	 *            can be null (indicates no used files).
	 */
	public ParseResult(File file, Program program, Set<Message> errors, Set<Message> warnings, Set<File> usedfiles) {
		if (file == null) {
			throw new NullPointerException("file");
		}
		if (program == null) {
			System.out.println("warning: could not parse " + file);
		}
		this.file = file;
		this.program = program;
		if (errors != null) {
			this.errors = errors;
		}
		if (warnings != null) {
			this.warnings = warnings;
		}
		if (file.exists()) {
			this.lastParseTime = file.lastModified();
		}
		if (this.usedFiles != null) {
			this.usedFiles = usedfiles;
		}
	}

	/**
	 * @return last parse time, 0 if not yet parsed.
	 */
	public Long getLastParseTime() {
		return this.lastParseTime;
	}

	public Set<Message> getErrors() {
		return this.errors;
	}

	public Set<Message> getWarnings() {
		return this.warnings;
	}

	/**
	 *
	 * @return Program. null if program was not yet parsed (see
	 *         {@link #getLastParseTime}) or if the file could not be parsed
	 */
	public Program getProgram() {
		return this.program;
	}

	public File getFile() {
		return this.file;
	}

	public boolean isMas() {
		return Extension.MAS2G == Extension.getFileExtension(this.file);
	}

	/**
	 * @return all files that this file is referring to.
	 */
	public Set<File> getUsedFiles() {
		return this.usedFiles;
	}
}
