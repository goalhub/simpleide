/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;

import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import nl.tudelft.goal.SimpleIDE.EditManager;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IconFactory;

/**
 * Save all active editors.
 *
 * @author W.Pasman 20jun2011
 */
public class SaveAllFileAction extends GOALAction {
	private static final long serialVersionUID = 1420049762356144414L;

	public SaveAllFileAction() {
		setIcon(IconFactory.SAVE_ALL_TEXT.getIcon());
		setShortcut('S', InputEvent.SHIFT_DOWN_MASK);
		setDescription("Save all edited files");
	}

	@Override
	/**
	 * {@inheritDoc}
	 */
	public void eventOccured(final IDEState currentState, final Object evt) {
		boolean isUserEditing = true;
		try {
			getIDE().getMainPanel().getCurrentPanel();
		} catch (final GOALUserError e) {
			isUserEditing = false;
		}
		setActionEnabled(currentState.getViewMode() == IDEMainPanel.EDIT_VIEW && isUserEditing);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void execute(final TreeNode selectedNode, final ActionEvent e) throws GOALException {
		EditManager.getInstance().saveAll();
	}
}
