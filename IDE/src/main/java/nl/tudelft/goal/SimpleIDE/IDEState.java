/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE;

import java.awt.Component;
import java.util.List;

import goal.core.runtime.RuntimeManager;
import goal.tools.BreakpointManager;
import goal.tools.DebugRun;
import goal.tools.IDEGOALInterpreter;
import goal.tools.debugger.IDEDebugger;
import goal.util.Observable;
import goal.util.Observer;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;

/**
 * An abstract representation of the IDE state as far as relevant for actions.
 * It contains the selected files in the files panel, the selected mode (edit or
 * run mode), the selected component (editor of a file, etc), selected
 * introspector etc.
 *
 * The IDEstate is needed to easen the interaction with the actions, that do not
 * want to depend on the entire IDE
 *
 * @author W.Pasman 14jun2011
 */
public interface IDEState extends Observable<Observer<IDEState, Object>, IDEState, Object> {

	/**
	 * Get the base component of the IDE, as center point for dialogs etc.
	 *
	 * @return {@link Component} that can be used to center a dialog on.
	 *
	 */
	Component getRootComponent();

	/**
	 * @return get the currently selected nodes in process panel.
	 *
	 *
	 */
	List<? extends IDENode> getSelectedProcessNodes();

	// public void setSelectedNodes(List<? extends IDENode> selection);

	/**
	 * returns 0 for edit mode, 1 for debug mode.
	 */
	int getViewMode();

	/**
	 *
	 * @return the current runtime manager, or null if nothing is running.
	 */
	RuntimeManager<IDEDebugger, IDEGOALInterpreter> getRuntime();

	/**
	 * Sets a new Runtime manager
	 *
	 * @param runner
	 *            the new runner. Set to null when running stops and runtime has
	 *            been taken down. If the current runtime is not null when this
	 *            is called, this will call {@link RuntimeManager#shutDown()}
	 *            before setting the new value.
	 */
	void setRuntime(DebugRun runner);

	BreakpointManager getBreakpointManager();

	/**
	 * @return the selected {@link AbstractFileNode}s for File actions
	 */
	List<? extends AbstractFileNode> getSelectedFileNodes();
}
