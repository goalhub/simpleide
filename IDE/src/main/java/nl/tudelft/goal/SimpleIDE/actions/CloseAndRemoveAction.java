/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.exceptions.GOALException;
import nl.tudelft.goal.SimpleIDE.EditManager;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IdeFiles;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;
import nl.tudelft.goal.SimpleIDE.filenodes.SimpleFileNode;

/**
 * Close the active file editor in the main panel and remove the file from the
 * file system
 *
 * @author W.Pasman 20jun2011
 */
public class CloseAndRemoveAction extends GOALAction {
	/**
	 *
	 */
	private static final long serialVersionUID = 4866539772903134720L;

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		List<? extends AbstractFileNode> selection = currentState.getSelectedFileNodes();
		if (selection.isEmpty() || currentState.getSelectedFileNodes().get(0).getBaseFile() == null) {
			setActionEnabled(false);
			return;
		}

		setActionEnabled(currentState.getViewMode() == IDEMainPanel.EDIT_VIEW && currentState.getRuntime() == null);
		// FIXME further check?
		// && (nodeType == NodeType.MASFILE || nodeType == NodeType.MODFILE ||
		// nodeType == NodeType.PLFILE || nodeType == NodeType.TXTFILE));
	}

	/**
	 * Override so that we can show the warning that the user needs to update
	 * his files.
	 *
	 * @throws GOALException
	 * @throws GOALIncompleteGUIUsageException
	 * @throws GOALCommandCancelledException
	 */
	@Override
	protected void executeAll(ActionEvent evt) throws GOALException {
		// first get the selection, because remove will change it
		List<? extends AbstractFileNode> selection = getCurrentState().getSelectedFileNodes();
		super.executeAll(evt);
		if (selection.size() == 1) {
			showRemoveWarning(selection.get(0));
		} else {
			JOptionPane.showMessageDialog(getCurrentState().getRootComponent(),
					"Remember to edit" + " the remaining files to remove the references.");
		}
	}

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent ae) throws GOALException {
		File file = ((AbstractFileNode) selectedNode).getBaseFile();
		if (file != null) { // not all selected elements may be removable.
			IdeFiles.getInstance().remove(file);
			EditManager.getInstance().close(file);
		}
	}

	/**
	 * show warning after removing GOAL file f, that user needs to edit his MAS
	 *
	 * @param f
	 *            is file that was renamed
	 */
	private void showRemoveWarning(AbstractFileNode node) {
		if (node instanceof SimpleFileNode) {
			JOptionPane.showMessageDialog(getCurrentState().getRootComponent(),
					"Remember to edit" + " the agent file(s) manually to remove the references");
		}
	}

}
