/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import nl.tudelft.goal.SimpleIDE.EditManager;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IconFactory;
import nl.tudelft.goal.SimpleIDE.IdeFiles;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;

/**
 * Edit the selected file.
 *
 * @author W.Pasman 20jun2011
 */
public class EditAction extends GOALAction {
	private static final long serialVersionUID = -2168559998109917661L;

	public EditAction() {
		setIcon(IconFactory.EDIT_TEXT.getIcon());
		setDescription("Edit selected file"); //$NON-NLS-1$
	}

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		List<? extends AbstractFileNode> sel = currentState.getSelectedFileNodes();
		setActionEnabled(
				currentState.getViewMode() == IDEMainPanel.EDIT_VIEW && !sel.isEmpty() && isEditable(sel.get(0)));
	}

	private boolean isEditable(AbstractFileNode ideNode) {
		return ideNode.getBaseFile() != null;
	}

	@Override
	protected void execute(TreeNode node, ActionEvent ae) throws GOALException {
		AbstractFileNode selectedNode = (AbstractFileNode) node;
		File file = selectedNode.getBaseFile();
		if (file == null) {
			throw new GOALUserError("selected node is not editable");
		}
		if (!file.exists()) {
			file = selectedNode.getExpectedFile();
			FileUtilities.proposeToCreate(getIDE().getFrame(), file, IdeFiles.getInstance());
		}
		EditManager.getInstance().editFile(file);
	}

}
