/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.Resources;
import goal.tools.errorhandling.Warning;
import goal.tools.errorhandling.WarningStrings;
import goal.tools.errorhandling.exceptions.GOALUserError;
import nl.tudelft.goal.SimpleIDE.EditManager;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IdeFiles;
import nl.tudelft.goal.SimpleIDE.TextEditorInterface;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;
import nl.tudelft.goal.SimpleIDE.filenodes.MASNode;
import nl.tudelft.goal.SimpleIDE.filenodes.SimpleFileNode;

/**
 * Save file currently selected in files panel.
 *
 * @author W.Pasman 20jun2011
 */
public class SaveSelectedFileAction extends GOALAction {
	private static final long serialVersionUID = 8692157178430900751L;

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		if (currentState.getViewMode() == IDEMainPanel.EDIT_VIEW) {
			// We're in debug view. Process nodes can't be saved.
			setActionEnabled(false);
		} else {
			// We're in edit view.
			List<? extends AbstractFileNode> selection = currentState.getSelectedFileNodes();
			// No nodes selected
			if (selection.isEmpty()) {
				setActionEnabled(false);
				return;
			}
			// TODO: we get a list but only inspect first element?
			AbstractFileNode node = selection.get(0);
			setActionEnabled((node instanceof MASNode || node instanceof SimpleFileNode)
					&& EditManager.getInstance().isOpenEditor(node.getBaseFile()));
		}
	}

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent ae) throws GOALUserError {
		try {
			TextEditorInterface editor = EditManager.getInstance()
					.getEditor(((AbstractFileNode) selectedNode).getBaseFile());
			editor.save();
			IdeFiles.getInstance().getProgram(editor.getFile());
		} catch (IOException e) {
			new Warning(String.format(Resources.get(WarningStrings.FAILED_FILE_SAVE), selectedNode.toString()), e)
					.emit();
		}
	}
}
