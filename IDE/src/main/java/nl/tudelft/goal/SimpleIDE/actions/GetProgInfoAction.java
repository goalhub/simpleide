/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;

import javax.swing.tree.TreeNode;

import cognitiveKrFactory.InstantiationFailedException;
import goal.tools.errorhandling.exceptions.GOALActionFailedException;
import goal.tools.errorhandling.exceptions.GOALException;
import languageTools.codeanalysis.MasAnalysis;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;

/**
 * Show GOAL MAS program info.
 *
 *
 * @author W.Pasman 20jun2011
 */
public class GetProgInfoAction extends GOALAction {
	private static final long serialVersionUID = 363163136224108049L;

	public GetProgInfoAction() {
		setName("MAS Analysis"); //$NON-NLS-1$
	}

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		List<? extends AbstractFileNode> selection = currentState.getSelectedFileNodes();

		boolean userSelectedMas = !selection.isEmpty() && selection.get(0).getParentMASNode() != null;

		setActionEnabled(currentState.getViewMode() == IDEMainPanel.EDIT_VIEW && userSelectedMas);
	}

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent ae) throws GOALException {

		File theFile = ((AbstractFileNode) selectedNode).getParentMASNode().getBaseFile();

		try {
			System.out.println(new MasAnalysis(theFile).toString());
		} catch (InstantiationFailedException e) {
			throw new GOALActionFailedException("Failed to analyze the MAS Program " + theFile, e);
		}
	}

}
