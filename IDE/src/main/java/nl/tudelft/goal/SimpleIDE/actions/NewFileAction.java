/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.tree.TreeNode;

import goal.preferences.CorePreferences;
import goal.tools.errorhandling.Resources;
import goal.tools.errorhandling.Warning;
import goal.tools.errorhandling.WarningStrings;
import goal.tools.errorhandling.exceptions.GOALUserError;
import languageTools.utils.Extension;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IconFactory;
import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * Create new file. What kind of file depends on the selection in file panel.
 *
 * @author W.Pasman 20jun2011
 */
public class NewFileAction extends GOALAction {
	private static final long serialVersionUID = 2342385586668828028L;

	public NewFileAction() {
		setIcon(IconFactory.NEW_FILE.getIcon());
		setShortcut('N');
		setDescription("create new file and open it"); //$NON-NLS-1$
	}

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		setActionEnabled(currentState.getViewMode() == IDEMainPanel.EDIT_VIEW);
	}

	@Override
	protected void execute(TreeNode node, ActionEvent e) throws GOALUserError {
		File file = FileUtilities.askFile(getIDE().getMainPanel(), false, "Provide a name for a new file",
				JFileChooser.FILES_ONLY, Extension.MOD2G.toString(), null, CorePreferences.getAgentBrowsePath(), false);
		if (file != null) {
			try {
				if (CorePreferences.getRememberLastUsedAgentDir()) {
					CorePreferences.setAgentBrowsePath(file.getParent());
				}
				FileUtilities.createFileAndEdit(file, IdeFiles.getInstance());
				// select new file for editing
			} catch (IOException ex) {
				new Warning(Resources.get(WarningStrings.FAILED_FILE_OPEN1), ex).emit();
			}
		}
	}

}
