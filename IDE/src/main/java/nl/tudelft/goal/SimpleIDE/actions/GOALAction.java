/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.Resources;
import goal.tools.errorhandling.Warning;
import goal.tools.errorhandling.WarningStrings;
import goal.tools.errorhandling.exceptions.GOALBug;
import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import goal.util.Observer;
import nl.tudelft.goal.SimpleIDE.ActionFactory;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDENode;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.IDEfunctionality;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;

/**
 * General GOAL action. Intended for use in the IDE only. The function
 * {@link #setIDEfunctionality(IDEfunctionality)} should be called once to
 * initialize this class for use. <br>
 * <p>
 * Actions should implement the Observer<IDEState,Object> and adjust their
 * visibility properly when such events happen.
 * <p>
 * NOTICE: actions MUST be stateless because {@link ActionFactory} caches
 * instances of this.
 *
 * @author W.Pasman 15jun2011
 */
public abstract class GOALAction extends AbstractAction implements Observer<IDEState, Object> {

	private static String action = "Action"; //$NON-NLS-1$

	/**
	 * The default name given to an action is the name of the java class, without
	 * the string "Action" if the java class ends on that. You can use CamelCase,
	 * each upper case character will be pre-fixed with a space for the text in the
	 * menu.
	 */
	public GOALAction() {
		getCurrentState().addObserver(this);

		String defaultName = ""; //$NON-NLS-1$
		for (final char c : this.getClass().getSimpleName().toCharArray()) {
			if (c >= 'A' && c <= 'Z' && !defaultName.isEmpty()) {
				defaultName += " "; //$NON-NLS-1$
			}
			defaultName += c;
		}
		if (defaultName.endsWith(action)) {
			defaultName = defaultName.substring(0, defaultName.lastIndexOf(action));
		}
		putValue(NAME, defaultName);

	}

	protected IDEState getCurrentState() {
		return ActionFactory.getCurrentState();
	}

	protected IDEfunctionality getIDE() {
		return ActionFactory.getIDE();
	}

	/**
	 * default, enforce to use the equals() code
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Two GOAL Actions are the same if they are the same class. So two UndoActions
	 * are always the same. You should use the ActionFactory to ensure that
	 * currentState is consitent always.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj == null) {
			return false;
		}
		return (getClass() == obj.getClass());
	}

	@Override
	/**
	 * This function is called when the action is activated. We reroute it to
	 * {@link #execute} after a few checks. Do not overwrite
	 */
	public final void actionPerformed(final ActionEvent event) {
		try {
			executeAll(event);
		} catch (final GOALBug e) {
			new Warning(Resources.get(WarningStrings.HIT_GOAL_BUG), e).emit();
		} catch (final GOALUserError e) {
			// Report what the user did that we could not handle. FIXME don't
			// duplicate messages, does not help the user.
			new Warning(e.getMessage(), e.getCause()).emit();
		} catch (final GOALException e) { // catch remaining exceptions. Just hope
			// it's not too bad.
			new Warning(Resources.get(WarningStrings.GOAL_EXCEPTION), e).emit();
		} catch (final RuntimeException e) {
			// emergency catch, we should avoid these.
			new Warning(String.format(Resources.get(WarningStrings.RUNTIME_EXCEPTION), toString()), e).emit();
		}
		ActionFactory.broadcastStateChange();
	}

	/**
	 * Execute this action for all currently selected nodes (which is dependign on
	 * {@link #isFileAction()}. It is the default behaviour: iterate over the
	 * selected nodes and call execute for each selected node. You can override this
	 * if you want a different behaviour based on the total selection.
	 * <p>
	 * By default all Exceptions are passed straight through, which means that any
	 * exception will cancel the execute for the remaining nodes.
	 *
	 *
	 * @param event is the UI event that triggered this action
	 * @throws GOALCommandCancelledException
	 * @throws GOALIncompleteGUIUsageException
	 * @throws GOALException
	 */
	protected void executeAll(final ActionEvent event) throws GOALException {
		if (getCurrentState().getViewMode() == IDEMainPanel.EDIT_VIEW) {
			// user is in edit mode. Use the selected Editor nodes

			for (final AbstractFileNode selectedNode : getCurrentState().getSelectedFileNodes()) {
				execute(selectedNode, event);
			}
		} else {
			// user is in debug mode. Use the selected Process nodes

			for (final IDENode selectedNode : getCurrentState().getSelectedProcessNodes()) {
				execute(selectedNode, event);
			}
		}
	}

	/**
	 * public version of execute. do not overwrite. It calls the private execute and
	 * then forces broadcastStateChange after call.
	 *
	 * @param node a {@link TreeNode}
	 * @param evt
	 *
	 *             is the UI event that triggered this action
	 *
	 * @throws GOALException
	 * @throws GOALIncompleteGUIUsageException
	 * @throws GOALCommandCancelledException
	 */
	public final void Execute(final TreeNode node, final ActionEvent evt) throws GOALException {
		execute(node, evt);
		// we know / assume that the state always is current so we don't have to
		// ask IDE for new state.
		ActionFactory.broadcastStateChange();
	}

	/**
	 * This function is called when the user triggers the action, eg by pressing a
	 * button or selecting a menu. Override this with the implementation of the
	 * action executer. It has been checked that {@link #developmentEnvironment} and
	 * {@link #currentState} have been set properly when this is called.
	 * <p>
	 * If multiple nodes were selected by user, the execute call is done multiple
	 * times, one node at a time.
	 * <p>
	 *
	 * @param node is the node that needs treatment with this action. if user is in
	 *             the files panel, this will be be a {@link AbstractFileNode} , or
	 *             else a {@link IDENode}.
	 * @param evt
	 *
	 *             is the UI event that triggered this action
	 */
	protected abstract void execute(TreeNode node, ActionEvent evt) throws GOALException;

	/**
	 * sets the icon for this action. Normal use is to override the constructor and
	 * call this to set the action's icon.
	 *
	 * @param icon the icon to be used.
	 */

	public void setIcon(final ImageIcon icon) {
		putValue(SMALL_ICON, icon);

	}

	/**
	 * Get the current icon of this action
	 *
	 * @return ImageIcon of the current icon
	 */
	public ImageIcon getIcon() {
		return (ImageIcon) getValue(SMALL_ICON);
	}

	/**
	 * Associates pressing keyboard character in combination with Apply key or CTLR
	 * (Linux, Windows) with the {@link GOALAction}.
	 *
	 * @param keyboardshortcut Keyboard character.
	 */
	public void setShortcut(final char keyboardshortcut) {
		setShortcut(keyboardshortcut, 0);
	}

	/**
	 * sets the keyboard shortcut for this action, aka "accelerator". Note that
	 * shortcuts work through the MENU bar, so only actions that have been added to
	 * the menu can be accelerated this way. <br>
	 * This function adds the common 'mask' to the shortcut, depending on the OS. On
	 * OSX, this means the META key is to be pressed along with the shortcut (aka
	 * 'Apple' key), while on Linux and Windows the ctrl key is to be pressed along
	 * with the shortcut. <br>
	 * You can also specify additional masks for the shortcut. The shift key is the
	 * most common additional mask.
	 *
	 * @param keyboardshortcut is the key(s) to press to trigger the action. Should
	 *                         be upper case char, eg 'S'
	 * @param mask             is the modifier key, see
	 *                         {@link InputEvent#SHIFT_DOWN_MASK} eg when you want
	 *                         the shift key to be depressed along with the
	 *                         keyboardshortcut, you use
	 *                         {@link InputEvent#SHIFT_DOWN_MASK}
	 *
	 */
	public void setShortcut(final char keyboardshortcut, int mask) {
		// Set mask for keystroke
		int fullmask = 0;

		// On OSX we need apple-X for the menu shortcuts, on other platforms
		// ctrl-X.
		if (System.getProperty("os.name").toLowerCase().indexOf("mac os x") > -1) { //$NON-NLS-1$ //$NON-NLS-2$
			fullmask = InputEvent.META_DOWN_MASK | mask;
		} else {
			fullmask = InputEvent.CTRL_DOWN_MASK | mask;
		}

		mask = mask | InputEvent.META_DOWN_MASK;

		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(keyboardshortcut, fullmask));
	}

	/**
	 * set the name of the action. This name is used for the menu text. If not used,
	 * the default name (see {@link #GOALAction()} ) is used. If possible, try to
	 * use the default name to keep code and menu items match as close as possible.
	 *
	 * @param name is the name of the action
	 */
	public void setName(final String name) {
		putValue(NAME, name);
	}

	/**
	 * Set a longer description for the action. Used as tool tip text.
	 *
	 * @param descr
	 */
	public void setDescription(final String descr) {
		putValue(SHORT_DESCRIPTION, descr);
	}

	public String getDescription() {
		return (String) getValue(SHORT_DESCRIPTION);
	}

	/**
	 * Our local, threadsafe version of the enabled flag. We copy this to the real
	 * enabled using invokelater, if you use the setActionEnabled call. DO NOT USE
	 * setEnabled directly!
	 */
	private boolean isEnabled = false;

	/**
	 * The swing enabled bit will switch only after SWING has had CPU time. with
	 * isActionEnabled you can check the planned state as has been set with the last
	 * call to setActionEnabled
	 *
	 * @return
	 */
	public boolean isActionEnabled() {
		return this.isEnabled;
	}

	/**
	 * Direct calling setEnabled from a non-AWT thread is not OK, see #1859. To fix
	 * this we provide a thread-safe setActionEnabled. WARNING: do not try to call
	 * setEnabled directly. Unfortunately we can't override it and make it private.
	 * We can't even override because the implementation of setActionEnabled would
	 * be a problem (how to call the super class setEnabled from the Swing thread if
	 * we override it?)
	 *
	 * @param isEnabled new value for enablednesss
	 */
	public void setActionEnabled(final boolean newEnabled) {
		final AbstractAction action = this;
		this.isEnabled = newEnabled;

		SwingUtilities.invokeLater(() -> action.setEnabled(GOALAction.this.isEnabled));
	}

}
