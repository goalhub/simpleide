/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.tudelft.goal.SimpleIDE.filenodes;

import java.io.File;
import java.util.List;

import javax.swing.tree.TreeNode;

import nl.tudelft.goal.SimpleIDE.IdeFiles;

/**
 * The root node for any simple file that does not have children.
 *
 * @author W.Pasman 25aug15
 *
 */
public class SimpleFileNode extends AbstractFileNode {

	public SimpleFileNode(IdeFiles files, AbstractFileNode parent, File base) {
		super(files, parent, base);
		if (base == null) {
			throw new NullPointerException();
		}
	}

	@Override
	public List<TreeNode> getChildren() {
		return null;
	}

	@Override
	public String getName() {
		return getBaseFile().getName();
	}

	@Override
	public boolean isLeaf() {
		return true;
	}

}