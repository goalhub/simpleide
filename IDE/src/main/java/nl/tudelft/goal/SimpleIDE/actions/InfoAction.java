/**
 * GOAL interpreter that facilitates developing and executing GOAL multi-agent
 * programs. Copyright (C) 2011 K.V. Hindriks, W. Pasman
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.tudelft.goal.SimpleIDE.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.tree.TreeNode;

import goal.tools.errorhandling.exceptions.GOALException;
import goal.tools.errorhandling.exceptions.GOALUserError;
import nl.tudelft.goal.SimpleIDE.IDEMainPanel;
import nl.tudelft.goal.SimpleIDE.IDEState;
import nl.tudelft.goal.SimpleIDE.filenodes.AbstractFileNode;

/**
 * show the Info box. Currently only works for file info.
 *
 * @author W.Pasman 31aug15
 */
public class InfoAction extends GOALAction {
	private static final long serialVersionUID = -246457625336588161L;

	public InfoAction() {
		setShortcut('I');
	}

	@Override
	public void eventOccured(IDEState currentState, Object evt) {
		setActionEnabled(currentState.getViewMode() == IDEMainPanel.EDIT_VIEW);
	}

	@Override
	protected void execute(TreeNode selectedNode, ActionEvent ae) throws GOALException {
		File file = ((AbstractFileNode) selectedNode).getBaseFile();
		if (file == null) {
			return;
		}
		String message;
		try {
			message = "File: " + file.getCanonicalPath() + "\n";
			message += "size: " + file.length();
		} catch (IOException e) {
			throw new GOALUserError(e.getMessage(), e);
		}
		JOptionPane.showMessageDialog(getCurrentState().getRootComponent(), message);
	}

}
