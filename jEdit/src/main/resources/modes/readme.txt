These files are mode files for JEdit

Brief note about JEdit modes:
JEdit works by cutting the given text into fragments. These fragments are then
further cut recursively if you use DELEGATE.

So the critical part for these modes is to determine the start and end point
of the fragments. There are a few mechanisms for this


1. Cut a piece of text until the next given mark. Use SPAN
2. Cut a piece of text until the next newline. Use EOL_SPAN
3. Pass all remaining text into another DELEGATE: use SEQ

The main mechanism for cutting text structurally to pieces is the SPAN,
since the other two just pass all remaining text.

The main problem with SPAN is that it ALWAYS requires a definite END marker.


More notes:
* don't use newlines in SEQ_REGEXP expressions
* Apparently SEQ has higher precedence than KEYWORD.  